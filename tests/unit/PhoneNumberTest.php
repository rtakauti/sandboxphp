<?php

use Codeception\Specify;
use Rtakauti\Codewars\PhoneNumber;

class PhoneNumberTest extends \Codeception\Test\Unit
{
    use Specify;

    private $phoneNumber;
    protected function _before()
    {
        $this->phoneNumber = new PhoneNumber();
    }

    public function testCreatePhoneNumber()
    {
        $this->describe('PhoneNumber output', function () {
            $this->should('create format', function () {
                verify($this->phoneNumber->createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]))->equals('(123) 456-7890');
            });
        });
    }

}
