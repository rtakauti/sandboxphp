<?php

use Codeception\Specify;
use Codeception\Test\Unit;
use Rtakauti\HackerRank\FizzBuzz;

class FizzBuzzTest extends Unit
{
    use Specify;

    /**
     * @var UnitTester
     */
    protected $tester;
    private $expected;
    private $fizzBuzz;

    protected function _before()
    {
        $this->expected = [1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'FizzBuzz'];
        $this->fizzBuzz = new FizzBuzz();
    }

    public function testOutput(): void
    {
        $this->describe("FizzBuzz output ", function () {
            $this->should("be correct for execute", function () {
                verify($this->fizzBuzz->execute(15))->equals($this->expected);
            });

            $this->should("be correct for execute1", function () {
                verify($this->fizzBuzz->execute1(15))->equals($this->expected);
            });

            $this->should("be correct for execute2", function () {
                verify($this->fizzBuzz->execute2(15))->equals($this->expected);
            });
        });
    }
}
