<?php

class ArrayListOriginal implements Iterator, ArrayAccess, JsonSerializable
{
    public static function explode($delimiters, &$string, $allowDuplicates = true)
    {
        if (!is_string($string) && !is_array($string) && !($string instanceof ArrayListOriginal)) {
            throw new Exception('String is invalid.');
        }
        //$delimiters may be a String (1 delimiter), an ArrayListOriginal, an Array or a Regular Expression


        if ($delimiters instanceof ArrayListOriginal) {
            $x = new ArrayListOriginal();
            foreach ($delimiters as $delimiter) {
                $x->add($string = ArrayListOriginal::explode($delimiter, $string), false, true);
            }
            return $x;
        } else {
            if (is_array($delimiters)) {
                return ArrayListOriginal::explode(new ArrayListOriginal($delimiters), $string);//CHEAP
            } else {
                if (is_string($delimiters)) {
                    if (is_string($string)) {
                        return new ArrayListOriginal(explode($delimiters, $string));
                    } else {
                        if (is_array($string)) {
                            return ArrayListOriginal::explode($delimiters, new ArrayListOriginal($string));
                        } else {
                            if ($string instanceof ArrayListOriginal) {
                                $x = new ArrayListOriginal();
                                foreach ($string as $str) {
                                    $x->add(ArrayListOriginal::explode($delimiters, $str), false, true);
                                }
                                return $x;
                            }
                        }
                    }
                } else {
                    if (@preg_match($delimiters, null) !== false) {
                        //return new ArrayListOriginal(preg_split($delimiters, $string));
                    }
                }
            }
        }

        throw new Exception('Invalid Delimiters (Must be Array, ArrayListOriginal, Regular Expression or String)');
    }

    //Instance
    private $internal_array;
    private $classNames;

    private $position;

    public function __construct($className = null)
    {
        $this->position = 0;
        if ($className instanceof ArrayListOriginal) {
            $this->internal_array = $className->internal_array;
            if (isset($className->className)) {
                $this->classNames = $className->classNames;
            }
        } else {
            if (is_array($className)) {
                $this->internal_array = $className;//YAY
            } else {
                $this->internal_array = array();//Construct the array that the list is.
                if ($className !== null) {
                    $cnames = explode('|', $className);
                    foreach ($cnames as $cname) {
                        if (!class_exists($cname)) {
                            throw new Exception('Invalid Class "' . $cname . '"');
                        }
                    }
                    $this->classNames = $cnames;
                }
            }
        }
    }


    public function isValidClass($className)
    {
        if (!isset($this->classNames)) {
            return true;
        }
        foreach ($this->classNames as $name) {
            if (is_subclass_of($name, $className)) {
                return true;
            }
            if ($name === $className) {
                return true;
            }
        }
        return false;
    }

    public function isValidInstance($object)
    {
        if (!isset($this->classNames)) {
            return true;
        }
        foreach ($this->classNames as $name) {
            if (!($object instanceof $name)) {
                continue;
            }
            return true;
        }
        return false;
    }


    public function &get($index)
    {
        if (!is_int($index) && !is_string($index)) {
            throw new Exception('Index must be an integer or string.');
        }
        if (!isset($this->internal_array[$index])) {
            throw new Exception ('Index undefined');
        }
        return $this->internal_array[$index];
    }


    public function set($index, &$value)
    {
        if (!is_int($index) && !is_string($index)) {
            throw new Exception('Index must be an integer or string.');
        }
        if (!$this->isValidInstance($value)) {
            throw new Exception('Invalid Type.');
        }

        $this->internal_array[$index] = $value;
    }


    public function add($obj, $mergeLists = true, $allowDuplicates = false)
    {
        if ($obj instanceof ArrayListOriginal && $mergeLists) {
            $itemsAdded = 0;
            foreach ($obj as $objectInOtherList) {
                try {
                    $result = $this->add($objectInOtherList, $mergeLists,
                        $allowDuplicates);//This will also help error-check
                } catch (Exception $e) {
                    continue;
                }
                if ($result) {
                    $itemsAdded++;
                }
            }
            return $itemsAdded;
        } else {
            if (!$this->isValidInstance($obj)) {
                throw new Exception('Invalid Type.');
            }

            if (!$allowDuplicates && $this->contains($obj)) {
                return false;
            }
            array_push($this->internal_array, $obj);
        }
        return true;
    }


    public function remove(&$obj)
    {
        if (!$this->isValidInstance($obj)) {
            throw new Exception('Invalid Type.');
        }
        if (($key = array_search($obj, $this->internal_array)) !== false) {
            $this->removeByIndex($key);
            return true;
        } else {
            return false;
        }
    }


    public function removeByIndex($key)
    {
        if (is_int($key)) {
            array_splice($this->internal_array, $key, 1);
        } else {
            unset($this->internal_array[$key]);
        }
    }


    public function contains(&$obj)
    {
        return in_array($obj, $this->internal_array);
    }


    public function size()
    {
        return count($this->internal_array);
    }


    public function getAll()
    {
        return $this->internal_array;
    }


    public function isKeySet($key)
    {
        if (!isset($this->internal_array[$key])) {
            return false;
        }
        return true;
    }


    public function isEmpty()
    {
        return $this->size() > 0;
    }


    public function filter($function, &$required_result, $arguments = array(), $strict = false)
    {
        foreach ($this->internal_array as $key => $value) {
            $result = call_user_func_array(array($value, $function), $arguments);
            if ($strict && $result !== $required_result) {
                $this->removeByIndex($key);
            } else {
                if ($result != $required_result) {
                    $this->removeByIndex($key);
                }
            }
        }
        return $this;
    }


    public function getByFunctionVale($function, &$required_result, $arguments = array(), $strict = false)
    {
        foreach ($this->internal_array as $key => $value) {
            $result = call_user_func_array(array($value, $function), $arguments);
            if ($strict && $result !== $required_result) {
                continue;
            } else {
                if ($result != $required_result) {
                    continue;
                }
            }
            return $value;
        }
        return null;
    }


    public function createCopy()
    {
        return new ArrayListOriginal($this);
    }


    public function implode($delimiter = ', ')
    {
        return implode($delimiter, $this->internal_array);
    }

    public function implode_by_function($function, $delimiter = '', $arguments = array())
    {
        $x = '';
        $c = 0;
        foreach ($this->internal_array as $value) {
            $result = call_user_func_array(array($value, $function), $arguments);
            $x .= $result;
            if (++$c < count($this->internal_array)) {
                $x .= $delimiter;
            }
        }
        return $x;
    }

    function rewind()
    {
        $this->position = 0;
    }

    function current()
    {
        return $this->internal_array[$this->position];
    }

    function key()
    {
        return $this->position;
    }

    function next()
    {
        ++$this->position;
    }

    function valid()
    {
        return isset($this->internal_array[$this->position]);
    }

    public function offsetExists($offset)
    {
        return $this->isKeySet($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        $this->removeByIndex($offset);
    }

    public function jsonSerialize()
    {
        return $this->internal_array;
    }

    public function __toString()
    {
        return json_encode($this);
    }
}