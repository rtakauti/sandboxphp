<?php
// Simulation configuration
$config['uppercase'] = true;
 
$lambda = function ($first, $second) {
    return $first + $second;
};
 
$result_lambda = $lambda(2, 3);
echo $result_lambda;
// Resultado: 5
echo "<hr>";


$closure = function ($message) use ($config) {
    if(isset($config['uppercase']) && $config['uppercase'] == true) {
        $message = strtoupper($message);
    }
 
    return $message;
};
 
$result_closure =  $closure('Hello world');
echo $result_closure;
// Resultado: HELLO WORLD
echo "<hr>";



// Using as a callback
function firstWord($message, $callback) {
    $parts = explode(' ', $message);
 
    return $callback($parts[0]);
}
 
$result_callback = firstWord('Hello World', $closure);
echo $result_callback;
// Resultado: HELLO

$num = 7;