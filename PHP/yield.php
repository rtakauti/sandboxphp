<?php

function y() {
    $results = [];
    d(function ($val) use (&$results) {
        $results[] = $val;
    });
    return $results;
}

foreach (y() as $val) {
    echo $val, PHP_EOL;
}

function vetor()
{
    echo 'foi';
    return range(1, 100);
}

function retorno()
{
    foreach (vetor() as $item) {
//        sleep(1);
        yield $item;
    }
    sleep(1);
}

foreach (retorno() as $item) {
    echo $item . "\t";
}