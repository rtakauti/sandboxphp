<?php

$translation = [
  'Hello world!' => [
    'pt'    => 'Bonjour tout le monde!',
    'en_US' => 'Hallo Welt!',
    'pt_BR' => 'Ola Mundo',
  ],
];

if (!function_exists('gettext')) {
  function _($token, $lang = NULL) {
    global $translation;
    if (empty($lang)
        || !array_key_exists($token, $translation)
        || !array_key_exists($lang, $translation[$token])
    ) {
      return $token;
    }
    else {
      return $translation[$token][$lang];
    }
  }
}
echo _('Hello World!');