<?php

spl_autoload_register(function ($filename) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . $filename . '.php';
});

use WeatherForecast\CurrentConditionsDisplay;
use WeatherForecast\WeatherData;

$weather = new WeatherData();

$currentDisplay = new CurrentConditionsDisplay($weather);

$weather->setMeasurements(30,20,10);