<?php

namespace WeatherForecast;


interface DisplayElement
{
    public function display();
}