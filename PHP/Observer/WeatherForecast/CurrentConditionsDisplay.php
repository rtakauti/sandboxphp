<?php

namespace WeatherForecast;


class CurrentConditionsDisplay implements Observer, DisplayElement
{
    private $temperature;
    private $humidity;
    private $weatherData;

    public function __construct(Subject $weatherData)
    {
        $this->weatherData = $weatherData;
        $this->weatherData->registerObserver($this);
    }

    public function update($temperature, $humidity, $pressure)
    {
        $this->temperature = $temperature;
        $this->humidity    = $humidity;
        $this->display();
    }

    public function display()
    {
        echo 'Current Conditions: '.$this->temperature.' C degrees and '.$this->humidity.' % humidity';
    }

}