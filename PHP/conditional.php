<?php

function foo()
{
//    return null;
//    return '';
    return 'foo';
}

function bar()
{
//    return null;
//    return '';
    return 'bar';
}

function baz()
{
//    return '';
    return 'baz';
}

class Foo
{
    public function __invoke()
    {
        return 'foo';
    }
}

if ($foo = foo()) {
    echo $foo;
}
echo PHP_EOL;
if (($foo = foo()) && ($foo = bar())) {
    echo $foo;
}
echo PHP_EOL;

$foo = [];
if (($foo[] = foo() ? foo() : 0) || ($foo[] = bar())) {
    print_r($foo);
}
$foo = [];
if (($foo[foo()] = foo() ?: 0) || ($foo[foo()] = bar())) {
    print_r($foo);
}
echo $foo[foo()];
echo PHP_EOL;
$foo = [];
if (($foo[] = foo()) && ($foo[] = bar())) {
    print_r($foo);
}

$foo = [];
$inc = 100;
if ((($foo[++$inc] = foo()) && ($foo[++$inc] = bar())) || ($foo[++$inc] = baz())) {
    print_r($foo);
}

$foo = [];
if ((($foo[] = foo()) && ($foo[] = bar())) && ($foo[] = baz())) {
    print_r($foo);
}

$foo  = new Foo();
$bar  = [];
$fooo = 'fooo';
$baz  = null;
if (($bar[$foo()] = $fooo) || ($bar[] = $baz)) {
    print_r($bar);
}




