<?php

namespace Animal;

abstract class Feline extends Mammal
{
    use Constructable, Countable, Liveable;
}