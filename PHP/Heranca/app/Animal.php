<?php

namespace Animal;

abstract class Animal
{
    use Constructable, Countable, Liveable;
}

