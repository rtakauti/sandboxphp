<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;


final class ArrayFilter
{
    public function getOodNumber($l, $r): array
    {
        return array_values(array_filter(range($l, $r), static function ($n) {
            return $n % 2 !== 0;
        }));
    }
}

final class ArrayFilterTest extends TestCase
{
    public function testGetOodNumber(): void
    {
        $arr = [3, 5];
        $arrayFilter = new ArrayFilter();
        $this->assertEquals($arr, $arrayFilter->getOodNumber(2, 5));
    }
}
