<?php
date_default_timezone_set('America/Sao_Paulo');
microtime(TRUE);
$date = date_create_from_format('d/m/y', '12/02/17');
$date = DateTime::createFromFormat('d/m/y', '12/02/17');
echo $date->format( 'Y/m/d' ).PHP_EOL;

$date = date_create_from_format('Y-m-d\TH:i:sP', '2017-02-10T10:10:10-09:00');
echo $date->format( 'd/m/Y' ).PHP_EOL;

echo '<pre>';
var_dump(DateTime::createFromFormat('d/m/y', '12/02/2017', new DateTimeZone('America/Sao_Paulo'))?'foi':'não foi');
$date1 = DateTime::createFromFormat('d/m/y', '01/03/17', new DateTimeZone('America/Sao_Paulo'));
$date = DateTime::createFromFormat('U.u', microtime(TRUE));
var_dump($date);

var_dump($date->diff($date1)->days);

var_dump(date_diff(new DateTime("now"), $date1)->days);

$date = date_create_from_format('Y-m-d\TH:i:s.uP', '2017-03-16T14:12:27.490745-03:00');
$date = date_create_from_format('Y-m-d\TH:i:s.uP', '2018-03-12T18:13:28.361-03:00');
echo $date->format( 'd/m/Y' ).PHP_EOL;

echo date('Y-m-d\TH:i:s.uP');
echo '<br>';
echo date('Y');