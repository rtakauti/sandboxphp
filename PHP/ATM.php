<?php

function withdraw1(int $value): array
{
    $bills = [100, 50, 20, 10, 5, 2,];
    $qtyBills = [];
    foreach ($bills as $bill) {
        if ($value >= $bill) {
            $qtyBills[$bill] = (int)($value / $bill);
            $value = (int)($value % $bill);
        }
    }
    return $qtyBills;
}

function withdraw(int $value, int $index = 0): array
{
    $bills = [100, 50, 20, 10, 5, 2,];
    static $qtyBills = [];
    if ($value < $bills[$index]) {
        withdraw($value, ++$index);
    }elseif (isset($bills[$index])) {
        $qtyBills[$bills[$index]] = (int)($value / $bills[$index]);
        withdraw((int)($value % $bills[$index]), ++$index);
    }
    return $qtyBills;
}
echo '<pre>';
print_r(withdraw(82));
