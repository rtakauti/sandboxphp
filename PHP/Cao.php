<?php

namespace Test;

use stdClass;

trait Cachorro
{
    function getLatir()
    {
        return $this->attributes->latir;
    }


    public function setLatir($latir)
    {
        $this->attributes->latir = $latir;
    }

    public function setComer($comer)
    {
        $this->attributes->comer = $comer;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }
}

class Cao
{
    private $attributes;

    public function __construct()
    {
        $this->attributes = new stdClass;
    }

    use Cachorro;

}

namespace Test1;

use Test\Cao;

$cao = new Cao();
$cao->setLatir('auau');
$cao->setComer('ração');
echo $cao->getLatir() . PHP_EOL;
//print_r($cao->getAttributes()) . PHP_EOL;
//echo $cao->latir . PHP_EOL;
print_r($cao);
