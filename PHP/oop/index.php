<?php

use Banco\Conta;
use Banco\ContaCorrentePlus;

spl_autoload_register(function ($filename) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . $filename . '.php';
});

try {
    $account = new Conta(100);
    echo $account->getBalance() . PHP_EOL;
    $account->deposit(-100);
} catch (LogicException $exception) {
    echo $exception->getMessage() . PHP_EOL;
}

$corrente = new ContaCorrentePlus(200);
try {
    try {
        $corrente->withdraw(300);
        echo $corrente->isUsingSpecial() . PHP_EOL;
        echo $corrente->isRed() . PHP_EOL;
        $corrente->withdraw(100);
        echo $corrente->isUsingSpecial() . PHP_EOL;
        echo $corrente->isRed() . PHP_EOL;
    } catch (LogicException $exception) {
        echo $exception->getMessage() . PHP_EOL;
//        throw $exception;
    }
} catch (Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
}

$corrente1 = new ContaCorrentePlus(0);
try {
    echo $corrente1->isUsingSpecial() . PHP_EOL;
    echo $corrente1->isRed() . PHP_EOL;

} catch (LogicException $exception) {
    echo $exception->getMessage() . PHP_EOL;
}

$days = &$corrente1->getDays();
echo $days. PHP_EOL;
$days = 5;
echo $corrente1->getDays() . PHP_EOL;