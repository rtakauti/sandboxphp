<?php

namespace Banco;

use LogicException;

class Conta
{

    protected $balance;

    public function __construct($value)
    {
        $this->isNegative($value);
        $this->balance = $value;
    }

    public function deposit($value)
    {
        $this->isNegative($value);
        $this->balance += $value;
    }


    public function withdraw($value)
    {
        $this->balance -= $value;
    }

    public function getBalance()
    {
        return $this->balance;

    }

    private function isNegative($value)
    {
        if ($value < 0) {
            throw new LogicException('Valor negativo');
        }
    }


}