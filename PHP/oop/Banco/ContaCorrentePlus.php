<?php

namespace Banco;

use LogicException;

class ContaCorrentePlus extends Conta
{

    private static $days;

    public function __construct($value)
    {
        self::$days = 2;
        parent::__construct($value);
    }


    public function withdraw($value)
    {
        if ($this->balance < $value) {
            self::$days -= 1;
        }
        $this->balance -= $value;
    }

    public function isUsingSpecial()
    {
        if (self::$days !== 2) {
            return 'Esta usando Especial';
        }
        return 'Não esta usando Especial';
    }

    public function isRed()
    {
        if (self::$days <= 0) {
            throw new LogicException('Passou do limite do Especial');
        }
    }

    public function &getDays()
    {
        return self::$days;
    }


}