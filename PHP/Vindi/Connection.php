<?php
declare(strict_types=1);

namespace Rtakauti\Vindi;

/**
 * @property \Rtakauti\Vindi\Customer customer
 */
class Connection
{

    /**
     * @param string $name
     * @return Vindi
     */
    public function __get(string $name): Vindi
    {
        $class = 'Rtakauti\\Vindi\\'.ucfirst($name);
        return new $class;
    }

}
