<?php
declare(strict_types=1);

namespace Rtakauti\Vindi;

class Customer implements Vindi
{

    public final function getName(): string
    {
        return 'Rubens';
    }

}