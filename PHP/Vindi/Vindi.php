<?php
declare(strict_types=1);

namespace Rtakauti\Vindi;


interface Vindi
{
    public function getName(): string ;
}