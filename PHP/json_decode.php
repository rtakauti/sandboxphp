<?php

$json = '{
    "doBookingResult": {
        "BookingNumber": 29316,
        "ClientID": 1819827566,
        "ClientName": "Atlantica agência",
        "OperatorName": "Atlantica Hoteis",
        "NumberOfBookingRooms": 1,
        "BaseID": 0,
        "CNPJ": "0",
        "Rooms": {
            "HotelBookingRoomResult_i": {
                "CancelationBlockWS": false,
                "BoardDescription": "n/d",
                "BreakfastIncluded": false,
                "HotelCategory": "0",
                "CityId": 1010502,
                "AgencyCommission": 0,
                "CancellationDate": "0001-01-01T00:00:00",
                "CheckInDate": "2018-04-01T00:00:00",
                "PaymentDeadline": "2018-03-07T21:00:00",
                "ReservationDate": "2018-03-02T14:39:16.3684558-03:00",
                "HotelAddress": "Av. Cidade Jardim  625",
                "HotelId": 54546,
                "ServiceId": 27903,
                "Location": "Iguatemi São Paulo, São Paulo, Brasil,Jockey Club de São Paulo, São Paulo, Brasil,Esporte Clube Pinheiros, São Paulo, Brasil,Parque Cidade Jardim, São Paulo, Brasil,Shopping Eldorado, São Paulo, Brasil,Shopping Vila Olímpia, São Paulo, Brasil,Prefeitura d",
                "LogXMLAuditId": "76925d1d-fdbc-49b3-9244-ce50db80649f",
                "AgencyCurrencyCommission": "BRL",
                "CityName": "São Paulo, Brasil",
                "HotelName": "Radisson Blu São Paulo",
                "NumberOfAdults": 1,
                "NumberOfChildren": 0,
                "NumberOfNights": 3,
                "Observation": "Fees not included (fees are to be paid at the hotel directly): MandatoryTax - BRL 6.60      <p><b>Informe-se antes de ir</b> <br /><ul>  <li>Pais ou um responsável viajando com crianças de até 18 anos devem apresentar a certidão de nascimento da criança e identidade com foto (como o passaporte, por exemplo) no momento do check-in. Para viagens para fora do Brasil, se apenas um dos pais estiver viajando com a criança, ele deverá apresentar uma carta de autorização de viagem assinada pelos pais e com reconhecimento de assinaturas em cartório, além da certidão de nascimento da criança e identidade com foto. No caso em que os pais ou responsável, se aplicável, não puderem ou não estiverem dispostos a assinar essa autorização, será necessária uma autorização judicial. As pessoas que pretendem viajar com crianças devem consultar o consulado brasileiro antes da viagem para mais informações. </li><li>Este estabelecimento oferece transporte do aeroporto (possível sobretaxa). Os hóspedes devem entrar em contato com o estabelecimento com os detalhes da chegada antes de viajarem. As informações de contato estão presentes na confirmação da reserva. </li><li>Crianças de até 7 anos se hospedam de graça quando ocupam o quarto dos pais ou guardiães, usando as camas existentes. </li> </ul></p><p><b>Taxas</b> <br /><p>Os seguintes depósitos e taxas são cobrados pelo hotel no momento do serviço prestado, durante o check-in ou check-out. </p> <ul>      <li>Taxa de traslado do aeroporto: BRL 140.00 por quarto (só ida)</li>    <li>Taxa para estacionamento com manobrista: BRL 28.00 a diária</li>         <li>Taxa para cama dobrável: BRL 60.00 por noite</li>            </ul> <p>A lista acima pode estar incompleta. As taxas e os depósitos podem não incluir impostos e estão sujeitos a mudanças. </p></p><p><b>Impostos e taxas obrigatórios</b> <br /><p>Você deverá pagar os seguintes encargos no estabelecimento:</p> <ul><li>A cidade cobra um imposto de BRL 2.20 por pessoa, por noite</li></ul> <p>Incluímos todas as cobranças que o estabelecimento nos forneceu. No entanto, as cobranças podem variar com base, por exemplo, na duração da estadia ou no tipo de quarto reservado. </p></p><p><b>Reformas e fechamento</b> <br />As seguintes instalações permanecerão fechadas de 13 de fevereiro de 2018 a 16 de fevereiro de 2018 (datas sujeitas a alteração):<ul><li>Piscina</li></ul></p> O serviço de traslado de/para o aeroporto está disponível 24 horas por dia, em horários agendados. Entre em contato com o estabelecimento antes para agendar o horário.",
                "VoucherObservation": "\n ",
                "CountryId": "BR",
                "Paxs": {
                    "Pax": {
                        "AddressNumber": 203,
                        "DDI": 55,
                        "DDD": 11,
                        "PhoneNumber": 997796683,
                        "Birthday": "1994-12-14T00:00:00",
                        "Name": "Lucas",
                        "LastName": "Ramos",
                        "Age": 23,
                        "EnumTitle": "Mr",
                        "EnumPaxType": "Main"
                    }
                },
                "NetPrice": {
                    "Currency": "BRL",
                    "Value": "7066.06666666667"
                },
                "OperatorCancellationPolicies": {
                    "CancellationPolicy": {
                        "StartDate": "2018-03-29T00:00:00-03:00",
                        "EndDate": "2018-04-01T00:00:00",
                        "Price": {
                            "Currency": "BRL",
                            "Value": "2161.13"
                        }
                    }
                },
                "ReservationId": 29316,
                "RoomDescription": "Suíte Royal, 1 cama King, Para não fumantes",
                "PaymentStatus": "NotPayed",
                "SystemStatus": "Confirmed",
                "ConversionRate": 1,
                "HotelPhone": "+551121335960",
                "ProviderID": 0,
                "ProviderName": "",
                "ProviderLogo": "",
                "ProviderBookingCode": "",
                "PaymentDate": "0001-01-01T00:00:00",
                "PaymentType": "OTHER",
                "Remarks": "",
                "CreationUserDetail": {
                    "Name": "Atlantica agencia",
                    "Id": 195854,
                    "UserName": "atlanticaagencia",
                    "Email": "teste@t4w.com.br"
                },
                "LastUpdateUserDetail": {
                    "Name": "Atlantica agencia",
                    "Id": 195854,
                    "UserName": "atlanticaagencia",
                    "Email": "teste@t4w.com.br"
                },
                "Payments": {},
                "TotalTaxSale": {
                    "Currency": "BRL",
                    "Value": "272.76"
                },
                "Taxes": {
                    "RoomTax_a": {
                        "Price": {
                            "Currency": "BRL",
                            "Value": "0"
                        },
                        "Description": "TaxAndServiceFee"
                    }
                },
                "RoomClientReference": "",
                "UrlVoucher": "http://homolog.cangooroo.net/Voucher/VoucherNovo.aspx?ReservaID=WzNmh4%2b6ZZl05uPrls9HquIWiHtyI65OJFSq3lvw8GY%3d&ServicoId=ksMRzgYq3H9gRtClOWUdadAe7MjBWEnZwncl%2bbNNTL8%3d&TipoServico=f%2fn7aDWZSOC%2bmRKOYhrhrDXDosrsQTdj2IxGEPJpJ9o%3d&u=KuhUPMozaoszbxUl3AgZdNMTb73SCGNDXmP4Elevkrq8mKab6xt8UQ%3d%3d&s=AqQg6Qtdel6d2zzqKYB4cnnaIZGWsozlhCew6qjvBXA=",
                "HotelCNPJ": "",
                "PaymentConvertionRate": 0,
                "PaymentCurrency": "BRL"
            }
        },
        "Transfers": {},
        "Tours": {},
        "Cars": {},
        "TrainTickets": {},
        "TrainPass": {},
        "Insurances": {},
        "TravelPackages": {},
        "Circuito": {},
        "Flights": {}
    }
}';

print_r(json_decode($json, true)['doBookingResult']['BookingNumber']);