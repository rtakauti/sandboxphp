<?php
declare(strict_types=1);

namespace Rtakauti\HackerRank;

use Generator;

/**
 * Class FizzBuzz
 * @package Rtakauti\HackerRank
 * @link https://www.hackerrank.com/challenges/fizzbuzz/problem
 */
class FizzBuzz
{
    /**
     * @var array
     */
    private $result = [];

    /**
     * @param int $lastNumber
     *
     * @return array
     */
    public function execute(int $lastNumber): array
    {
        for ($number = 1; $number <= $lastNumber; $number++) {
            $value = $number;
            if (!empty($all = (($number % 3 === 0) ? 'Fizz' : '') . (($number % 5 === 0) ? 'Buzz' : ''))) {
                $value = $all;
            }
            $this->result[] = $value;
        }
        return $this->result;
    }

    /**
     * @param int $lastNumber
     *
     * @return array
     */
    public function execute1(int $lastNumber): array
    {
        return array_map(static function ($number) {
            if (empty($all = (($number % 3 === 0) ? 'Fizz' : '') . (($number % 5 === 0) ? 'Buzz' : ''))) {
                return $number;
            }
            return $all;
        }, range(1, $lastNumber));
    }

    /**
     * @param int $lastNumber
     *
     * @return array
     */
    public function execute2(int $lastNumber): array
    {
        foreach ($this->generateNumbers($lastNumber) as $number) {
            $value = $number;
            if (!empty($all = (($number % 3 === 0) ? 'Fizz' : '') . (($number % 5 === 0) ? 'Buzz' : ''))) {
                $value = $all;
            }
            $this->result[] = $value;
        }
        return $this->result;
    }

    /**
     * @param int $lastNumber
     *
     * @return Generator|null
     */
    private function generateNumbers(int $lastNumber): ?Generator
    {
        for ($number = 1; $number <= $lastNumber; $number++) {
            yield $number;
        }
    }
}
