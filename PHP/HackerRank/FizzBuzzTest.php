<?php
declare(strict_types=1);

namespace Rtakauti\HackerRank;

use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    private FizzBuzz $fizzBuzz;
    private array $expected;

    public function setUp(): void
    {
        $this->expected = [1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz', 11, 'Fizz', 13, 14, 'FizzBuzz'];
        $this->fizzBuzz = new FizzBuzz();
        parent::setUp();
    }

    public function testExecute(): void
    {
        $this->assertEquals($this->expected, $this->fizzBuzz->execute(15));
    }

    public function testExecute1(): void
    {
        $this->assertEquals($this->expected, $this->fizzBuzz->execute1(15));
    }

    public function testExecute2(): void
    {
        $this->assertEquals($this->expected, $this->fizzBuzz->execute2(15));
    }
}
