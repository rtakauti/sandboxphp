<?php
$callback = function ($num) {
    $result        = new stdClass();
    $result->teste = $num;
    
    return $result;
};

$callback_array = function (...$nums) {
    $result = new stdClass();
    foreach ($nums as $num) {
        $result->{'teste' . $num} = $num + 10;
    }
    
    
    return $result;
};

$array = [
    'parceiro' => function () {
        $result        = new stdClass();
        $result->teste = 1;
        
        return $result;
    },
    'fatura'   => 'callback',
    'loja'     => 'callback_array',
];


function callback($callback)
{
    return $callback();
}

function keys($key, $array)
{
    return $array[$key];
}

function key1($key, $array)
{
    global $callback;
    
    return ${$array[$key]}(100);
}

function key2($key, $array)
{
    global $callback;
    
    return ${$array[$key]};
}

function key3($key, $array)
{
    global $callback_array;
    
    return ${$array[$key]};
}

//print_r(callback($callback));
//$function = keys('parceiro', $array);
//print_r($array);
//print_r($function());
//print_r(key1('fatura', $array));
//print_r(key2('fatura', $array)(111));
print_r((array)key3('loja', $array)(1, 2, 3, 4));




