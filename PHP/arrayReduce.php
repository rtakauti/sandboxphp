<?php
$object1      = new stdClass();
$object1->num = 1;
$array[]      = $object1;
$object2      = new stdClass();
$object2->num = 2;
$array[]      = $object2;
$object3      = new stdClass();
$object3->num = 3;
$array[]      = $object3;
$object4      = new stdClass();
$object4->num = 4;
$array[]      = $object4;

$object = array_reduce($array, function ($a, $b) {
    echo 'a: ' . print_r($a);
    echo PHP_EOL;
    echo 'b: ' . print_r($b);
    echo PHP_EOL;
    
    if ($a === null) {
        return $b;
    }
    
    return $a->num > $b->num ? $a : $b;
});

print_r($object);

$test = array_reduce([], function ($a, $b) {
    return $a + $b;
});
echo 'test'.PHP_EOL;
var_dump($test);
