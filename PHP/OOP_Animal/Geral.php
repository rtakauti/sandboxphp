<?php

namespace StudioVisual\Constants;

if ( ! defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if ( ! defined('ROOT')) {
    define('ROOT', __DIR__ );
}

