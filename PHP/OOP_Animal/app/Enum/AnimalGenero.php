<?php

namespace StudioVisual\Enum;


abstract class AnimalGenero
{

    const MASCULINO = 1;
    const FEMININO = 2;

}