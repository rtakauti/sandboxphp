<?php

namespace StudioVisual\Contracts;


interface MamiferoInterface
{
    public function amamentar();

    public function mamar();
}