<?php


namespace StudioVisual\Contracts;


use StudioVisual\Animais\AnimalAbstract;

interface RacaInterface
{
   public function setRaca(AnimalAbstract $animal);

}