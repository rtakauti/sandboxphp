<?php


namespace StudioVisual\Support;


abstract class SinfoniaAnimal
{
    public static function tocar(array $animais)
    {
        foreach ($animais as $animal) {
            echo $animal->comunicar() . PHP_EOL;
        }
    }
}