<?php

namespace StudioVisual\Animais;


use StudioVisual\Traits\OviparoTrait;

class Pato extends AnimalAbstract
{
    protected static $quantidade = 0;

    use OviparoTrait;
    
    public function locomover()
    {
        return 'nadar, voar, andar';
    }
    
    public function comunicar()
    {
        return 'Quaquauqa';
    }
}