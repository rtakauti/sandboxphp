<?php

namespace StudioVisual\Animais;

use LogicException;
use StudioVisual\Contracts\MamiferoInterface;
use StudioVisual\Contracts\RacaInterface;
use StudioVisual\Traits\CachorroRacaTrait;
use StudioVisual\Traits\MamiferoTrait;
use StudioVisual\Traits\ViviparoTrait;

class CaninoAbstract extends AnimalAbstract implements MamiferoInterface
{
    
    protected static $quantidade = 0;
    
    use  MamiferoTrait, ViviparoTrait, CachorroRacaTrait;
    

    public function locomover()
    {
        return 'Andando';
    }
    
    /**
     * @return array
     */
    public function reproduzir()
    {
        try {
            $filhotes = parent::reproduzir();
        } catch (LogicException $exception) {
            throw $exception;
        }
        if ( ! empty($filhotes)) {
            $nomes   = ['Bingo', 'Billy', 'Rex', 'Lessie', 'Potty', 'Pingo', 'Spot', 'Lady', 'Laika', 'Bob', 'Bolt'];
            $generos = [1, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1];
            foreach ($filhotes as $num => $filhote) {
                $filhote->setNome($nomes[$num])
                        ->setGenero($generos[$num])
                        ->setNomeMae($this->getNome());
            }
            
            return $filhotes;
        }
    }
    
    
    public function setRaca(RacaInterface $raca)
    {
        $raca->setRaca($this);
    }
    
    
    public function comunicar()
    {
        return 'Auauau';
    }
}