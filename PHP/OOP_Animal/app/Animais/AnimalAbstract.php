<?php

namespace StudioVisual\Animais;

use stdClass;
use StudioVisual\Contracts\AnimalInterface;
use StudioVisual\Enum\AnimalGenero;
use StudioVisual\Exceptions\AcasalarException;
use StudioVisual\Exceptions\MorrerException;

abstract class AnimalAbstract implements AnimalInterface
{
    protected static $quantidade = 0;
    protected $attributes;

    public function __construct()
    {
        $this->attributes         = new stdClass;
        $this->attributes->genero = random_int(AnimalGenero::MASCULINO, AnimalGenero::FEMININO);
        $this->attributes->isVivo = true;
        ++static::$quantidade;
        ++self::$quantidade;

    }

    public function respirar()
    {
        return 'respirando';
    }

    public function comer()
    {
        return 'Comendo';
    }

    public function beber()
    {
        return 'Bebendo';
    }


    /**
     * @return int
     */
    public static function mostrarQuantidade()
    {
        return static::$quantidade;
    }


    /**
     * @param $genero
     *
     * @return $this
     */
    public function setGenero($genero)
    {
        $this->attributes->genero = $genero;

        return $this;
    }


    /**
     * @param AnimalInterface $animal
     *
     * @return string
     */
    public function acasalar(AnimalInterface $animal)
    {
        if ($this->attributes->genero === $animal->attributes->genero) {
            throw new AcasalarException();
        }
        $this->setParceiro($animal);
        $this->attributes->parceiro->setParceiro($this);

        return 'acasalou';
    }


    /**
     * @return array
     */
    public function reproduzir()
    {
        $isFeminino  = AnimalGenero::FEMININO === $this->attributes->genero;
        $hasParceiro = isset($this->attributes->parceiro);
        if ($isFeminino && $hasParceiro) {
            $filhotes = [];
            foreach (range(0, random_int(1, 5)) as $num) {
                $filhotes[$num] = new static;
            }

            $this->attributes->parceiro->setParceiro(null);
            $this->attributes->parceiro = null;

            return $filhotes;
        }
        throw new \LogicException('Somente animais do gênero feminino podem reproduzir!!!');
    }

    /**
     *
     */
    public function morrer()
    {
        if ( ! $this->attributes->isVivo) {
            throw new MorrerException();
        }
        $this->attributes->isVivo = false;

        return 'Morreu';
    }

    /**
     * @return mixed
     */
    public function mostrarParceiro()
    {
        return $this->attributes->parceiro;
    }

    /**
     * @param $parceiro
     */
    public function setParceiro($parceiro)
    {
        $this->attributes->parceiro = $parceiro;
    }


    /**
     * @return mixed
     */
    abstract public function locomover();


}