<?php


namespace StudioVisual\Animais;


use StudioVisual\Traits\SelvagemTrait;

final class Mabeco extends CaninoAbstract
{
    
    protected static $quantidade = 0;
    
    public function __construct()
    {
        parent::__construct();
        ++parent::$quantidade;
    }
    
    use SelvagemTrait;
}