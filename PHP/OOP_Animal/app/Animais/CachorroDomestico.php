<?php


namespace StudioVisual\Animais;


use StudioVisual\Traits\DomesticoTrait;

class CachorroDomestico extends CaninoAbstract
{
    protected static $quantidade = 0;
    
    public function __construct()
    {
        parent::__construct();
        ++parent::$quantidade;
    }
    
    use DomesticoTrait;
}