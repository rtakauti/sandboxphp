<?php

namespace StudioVisual\Animais\Racas;


class Chihuahua extends CachorroRacaTemplate
{
    const MAX_TAMANHO = 20;
    const MIN_TAMANHO = 10;
    const MAX_PESO = 3;
    const MIN_PESO = 1;

}