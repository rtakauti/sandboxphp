<?php


namespace StudioVisual\Animais\Racas;


use ReflectionClass;
use StudioVisual\Animais\AnimalAbstract;
use StudioVisual\Contracts\RacaInterface;

abstract class CachorroRacaTemplate implements RacaInterface
{
    public function setRaca(AnimalAbstract $cachorro)
    {
        if ($cachorro->getTamanho() > static::MAX_TAMANHO) {
            $cachorro->setTamanho(static::MAX_TAMANHO);
        }
        if (empty($cachorro->getTamanho()) || $cachorro->getTamanho() < static::MIN_TAMANHO) {
            $cachorro->setTamanho(static::MIN_TAMANHO);
        }
        if ($cachorro->getPeso() > static::MAX_PESO) {
            $cachorro->setPeso(static::MAX_PESO);
        }
        $cachorro->setLatido('<audio controls> <source src="'.$this->getBase64('audios', 'mp3', 'data:audio/mpeg;base64,').'" type="audio/mpeg"> </audio>');
        $cachorro->setPhoto( '<img src="'.$this->getBase64('photos', 'jpg', 'data:image/jpg;base64,').'"/>');
    }

    public function getPath($type, $extension)
    {
        return ROOT.DS.'assets'.DS.$type.DS . (new ReflectionClass($this))->getShortName() . '.'.$extension;
    }

    public function getBase64($type, $extension, $mime)
    {
        $image = fread(fopen($this->getPath($type, $extension), 'rb'), filesize($this->getPath($type, $extension)));
        return  $mime. base64_encode($image);
    }
}