<?php


namespace StudioVisual\Traits;


trait MamiferoTrait
{
    public function amamentar()
    {
        return 'Amamentando';
    }
    
    public function mamar()
    {
        return 'Mamando';
    }
}