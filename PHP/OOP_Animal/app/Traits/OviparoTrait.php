<?php


namespace StudioVisual\Traits;


trait OviparoTrait
{
    public function botar()
    {
        return 'Botando';
    }
    
    public function nascerDoOvo()
    {
        return 'Nascendo do Ovo';
    }
}