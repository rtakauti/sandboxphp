<?php


namespace StudioVisual\Traits;


trait ViviparoTrait
{
    public function parir()
    {
        return 'Parindo';
    }
    
    public function nascerDoUtero()
    {
        return 'Nascendo do Utero';
    }
}