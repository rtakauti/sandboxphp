<?php

use StudioVisual\Animais\AnimalAbstract;
use StudioVisual\Animais\CachorroDomestico;
use StudioVisual\Animais\CaninoAbstract;
use StudioVisual\Animais\Mabeco;
use StudioVisual\Animais\Ornitorrinco;
use StudioVisual\Animais\Pato;
use StudioVisual\Animais\Racas\Chihuahua;
use StudioVisual\Enum\AnimalGenero;
use StudioVisual\Exceptions\MorrerException;
use StudioVisual\Support\SinfoniaAnimal;

include_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
echo '<pre>';

$pingo = new CachorroDomestico();
$pingo->setNome('Pingo');
$pingo->setGenero(AnimalGenero::MASCULINO);
$spot = new CachorroDomestico();
$spot->setNome('Spot');
$spot->setPeso(10);
$spot->setRaca(new Chihuahua);
$spot->setGenero(AnimalGenero::MASCULINO);
print_r($spot);
try {
    echo $spot->morrer() . PHP_EOL;
} catch (MorrerException $exception) {
    echo $exception;
}

echo '<br>';
try {
    echo $spot->morrer() . PHP_EOL;
} catch (MorrerException $exception) {
    echo $exception;
}
$rex = new CachorroDomestico();
$rex->setNome('Rex');
$rex->setGenero(AnimalGenero::MASCULINO);
$lessie = new CachorroDomestico();
$lessie->setNome('Lessie');
$lessie->setGenero(AnimalGenero::FEMININO);
echo $lessie . PHP_EOL;
echo $rex . PHP_EOL;
echo $lessie->locomover() . PHP_EOL;
try {
    echo $lessie->acasalar($rex) . PHP_EOL;
    echo $lessie->mostrarParceiro() . PHP_EOL;
} catch (LogicException $exception) {
    echo $exception->getMessage();
}

try {
    print_r($lessie->reproduzir());
} catch (Exception $exception) {
    echo $exception->getMessage();
}

$pato = new Pato();
$pato->setGenero(AnimalGenero::MASCULINO);
$pata = new Pato();
$pata->setGenero(AnimalGenero::FEMININO);
try {
    echo $pata->acasalar($pato) . PHP_EOL;
    print_r($pata->reproduzir());
    echo $pata->mostrarParceiro();
} catch (LogicException $exception) {
    echo $exception->getMessage();
}
$perry = new Ornitorrinco();
$mabeco = new Mabeco();

echo 'Ornitorrinco: ' . Ornitorrinco::mostrarQuantidade() . PHP_EOL;
echo 'Mabeco: ' . Mabeco::mostrarQuantidade() . PHP_EOL;
echo 'CachorroDomestico: ' . CachorroDomestico::mostrarQuantidade() . PHP_EOL;
echo 'Canino: ' . CaninoAbstract::mostrarQuantidade() . PHP_EOL;
echo 'Pato: ' . Pato::mostrarQuantidade() . PHP_EOL;
echo 'Total: ' . AnimalAbstract::mostrarQuantidade() . PHP_EOL . PHP_EOL;


// Exemplo de polimorfismo
function locomocao(AnimalAbstract $animal)
{
    echo $animal->locomover() . PHP_EOL;
}
locomocao($rex);
locomocao($pato);
locomocao($perry);

echo PHP_EOL . PHP_EOL . 'Sinfonia' . PHP_EOL;
$animais[] = $rex;
$animais[] = $pato;
$animais[] = $perry;
$animais[] = $lessie;
$animais[] = new Ornitorrinco();
$animais[] = new Mabeco();

SinfoniaAnimal::tocar($animais);

//echo  ROOT.'assets'.DIRECTORY_SEPARATOR.'photos'.DIRECTORY_SEPARATOR;
//echo  (new Chihuahua())->getBase64();
