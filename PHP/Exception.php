<?php

namespace Test;


class Exception
{
    public function test()
    {
        $test = true;
        $exception = new \Exception;
        if ($test) {
            $exception = new \HttpException;
        }
        throw $exception;
    }

    public function testar(){
        throw new \Exception;
    }
}

$test = new Exception();
$test->test();
$test->testar();
