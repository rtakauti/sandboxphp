<?php

namespace Alimentacao;

trait Carnivoro
{
    private $comida;

    public function getComida()
    {
        return implode(', ', $this->comida);
    }

    public function setComida()
    {
        $this->comida[] = AlimentacaoInterface::CARNE;
    }

}

trait Herbivoro
{
    private $comida;

    public function getComida()
    {
        return implode(', ', $this->comida);
    }

    public function setComida()
    {
        $this->comida[] = AlimentacaoInterface::VEGETAL;
    }

}

trait Onivoro
{
    use  Carnivoro, Herbivoro {
        Carnivoro::setComida as setCarne;
        Herbivoro::setComida as setVegetal;
        Herbivoro::getComida insteadof Carnivoro;
    }

    public function setComida()
    {
        $this->setCarne();
        $this->setVegetal();
    }
}

interface AlimentaInterface
{
    public function getComida();
}

interface TipoAlimentacaoInterface
{
    public function setAlimento();
}

interface AlimentacaoInterface extends AlimentaInterface, TipoAlimentacaoInterface
{
    const CARNE = 'carne';
    const VEGETAL = 'vegetal';
}

class TipoAlimentacao
{
    public function __construct()
    {
        $this->setComida();
    }
}


class Alimentacao extends TipoAlimentacao implements AlimentacaoInterface
{
    use Onivoro {
        Onivoro::setComida as protected;
    }

    public function setAlimento()
    {

    }
}

$alimentacao = new Alimentacao();
echo $alimentacao->getComida() . PHP_EOL;
print_r($alimentacao);