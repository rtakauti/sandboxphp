<?php

class MySimpleHeap extends SplHeap
{
    public function compare($value1, $value2)
    {
        return ($value1 - $value2);
    }
}

$obj = new MySimpleHeap();
$obj->insert(4);
$obj->insert(8);
$obj->insert(1);
$obj->insert(0);

foreach ($obj as $number) {
    echo $number . "\n";
}


$cache  = new CachingIterator(new ArrayIterator(range(1,100)), CachingIterator::FULL_CACHE);

foreach ($cache as $c) {

}

print_r($cache->getCache());