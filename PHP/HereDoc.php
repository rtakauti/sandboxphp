<?php

namespace Rtakauti;


class HereDoc
{
    public function print()
    {
        $count = count([1,2,3,4]);
        echo <<<HTML
<h1>$count</h1>
HTML;
    }
}

$hereDoc = new HereDoc;
$hereDoc->print();