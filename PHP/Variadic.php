<?php

namespace Rtakauti;


class Variadic
{
//    public function variadic()
//    {
//        print_r(func_get_args());
//    }


    public function variadic($n1, ...$var)
    {
        echo $n1 . "\n";
        print_r($var);
    }

}

$sum = function (...$var) {
    return array_sum($var);
};


echo $sum(1, 2, 3, 4, 5);


//$variadic = new Variadic;
//$variadic->variadic('test');
//$variadic->variadic(1, 2, 3, 4, 5);