<?php
class ISS extends TemplateDeImpostoCondicional {
    public function deveUsarMaximo(Orcamento $orcamento)
    {
        return $orcamento->getValor() > 500;
    }

    public function taxaMinima(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.1;
    }

    public function taxaMaxima(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.15;
    }
}