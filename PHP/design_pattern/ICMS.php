<?php

class ICMS extends TemplateDeImpostoCondicional
{
    public function deveUsarMaximo(Orcamento $orcamento)
    {
        return $orcamento->getValor() > 500;
    }

    public function taxaMinima(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.05;
    }

    public function taxaMaxima(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.15;
    }
}