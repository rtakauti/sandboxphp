<?php
class Desconto5Itens implements Desconto {

    private $nextDesconto;

    public function desconto(Orcamento $orcamento )
    {
        if (count($orcamento->getItens()) > 5){
            return $orcamento->getValor() * 0.1;
        }else{
            return $this->nextDesconto->desconto($orcamento);
        }
    }

    public function setNextDesconto(Desconto $desconto){
        $this->nextDesconto = $desconto;
    }
}