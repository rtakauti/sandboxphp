<?php

class CalculadoraDeDesconto
{

    public function desconto(Orcamento $orcamento)
    {
        $desconto5Itens = new Desconto5Itens();
        $desconto500Reais = new Desconto500Reais();
        $semDesconto = new SemDesconto();

        $desconto5Itens->setNextDesconto($desconto500Reais);
        $desconto500Reais->setNextDesconto($semDesconto);
        return $desconto5Itens->desconto($orcamento);
    }
}