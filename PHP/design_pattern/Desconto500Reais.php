<?php

class Desconto500Reais implements Desconto
{

    private $nextDesconto;

    public function desconto(Orcamento $orcamento)
    {
        if ($orcamento->getValor() > 500) {
            return $orcamento->getValor() * 0.05;
        } else {
            return $this->nextDesconto->desconto($orcamento);
        }
    }

    public function setNextDesconto(Desconto $desconto)
    {
        $this->nextDesconto = $desconto;
    }
}