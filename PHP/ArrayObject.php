<?php

class MyArrayObject extends ArrayObject {
    static $debugLevel = 2;

    static public function sdprintf() {
        if (static::$debugLevel > 1) {
            call_user_func_array("printf", func_get_args());
        }
    }

    public function offsetGet($name) {
        self::sdprintf("%s(%s)\n", __FUNCTION__, implode(",", func_get_args()));
        return call_user_func_array(array(parent, __FUNCTION__), func_get_args());
    }
    public function offsetSet($name, $value) {
        self::sdprintf("%s(%s)\n", __FUNCTION__, implode(",", func_get_args()));
        return call_user_func_array(array(parent, __FUNCTION__), func_get_args());
    }
    public function offsetExists($name) {
        self::sdprintf("%s(%s)\n", __FUNCTION__, implode(",", func_get_args()));
        return call_user_func_array(array(parent, __FUNCTION__), func_get_args());
    }
    public function offsetUnset($name) {
        self::sdprintf("%s(%s)\n", __FUNCTION__, implode(",", func_get_args()));
        return call_user_func_array(array(parent, __FUNCTION__), func_get_args());
    }
}

$mao = new MyArrayObject();
$mao["name"] = "bob";
$mao["friend"] = "jane";
print_r((array)$mao);
