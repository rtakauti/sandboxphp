<?php

class ArrayList implements IteratorAggregate, ArrayAccess, Countable, JsonSerializable
{

    private $items;
    private $position;

    public function __construct()
    {
        $this->position = 0;
        $this->items = [];
    }


    public function max($property = null)
    {
        return array_reduce($this->items, function ($a, $b) use ($property) {
            if($property)$method = 'get'.ucfirst($property);
            $c = is_object($a) ? $a->{$method}() : -INF;
            return $c > $b->{$method}() ? $a : $b;
        });
    }

    public function sort(callable $callback = null)
    {
        $item = $this->items;
        $callback ?
            uasort($item, $callback) :
            uasort($item, function ($a, $b) {
                if ($a === $b) {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            });
        return $item;
    }


    public function min($property = null)
    {
        return array_reduce($this->items, function ($a, $b) use ($property) {
            if($property)$method = 'get'.ucfirst($property);
            $c = is_object($a) ? $a->{$method}() : INF;
            return $c < $b->{$method}() ? $a : $b;
        });
    }

    public function get($key, $default = null)
    {
        if ($this->offsetExists($key)) {
            return $this->items[$key];
        }

        return $default;
    }


    public function set($index, $value)
    {
        $this->items[$index] = $value;
    }


    public function put($key, $value)
    {
        $this->offsetSet($key, $value);

        return $this;
    }


    public function forget($keys)
    {
        foreach ((array)$keys as $key) {
            $this->offsetUnset($key);
        }

        return $this;
    }


    public function contains($obj)
    {
        return in_array($obj, $this->items);
    }


    public function all()
    {
        return $this->items;
    }

    public function has($key)
    {
        return $this->offsetExists($key);
    }


    public function isEmpty()
    {
        return !$this->count() > 0;
    }


    public function values()
    {
        return array_values($this->items);
    }

    public function toJson()
    {
        return json_encode($this);
    }

    public function __toString()
    {
        return $this->toJson();
    }

    public function reverse()
    {
        return array_reverse($this->items, true);
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->items);
    }

    public function offsetGet($offset)
    {
        return $this->items[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function jsonSerialize()
    {
        return $this->items;
    }

    public function count()
    {
        return count($this->items);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    public function flip()
    {
        return array_flip($this->items);
    }

    public function pop()
    {
        return array_pop($this->items);
    }


    public function push($value)
    {
        $this->offsetSet(null, $value);
        return $this;
    }

    public function filter(callable $callback = null)
    {
        if ($callback) {
            $return = [];
            foreach ($this->items as $key => $value) {
                if ($callback($value, $key)) {
                    $return[$key] = $value;
                }
            }
            return $return;
        }
        return array_filter($this->items);
    }


    public function map(callable $callback)
    {
        $keys = array_keys($this->items);
        $item = array_map($callback, $this->items, $keys);
        return array_combine($keys, $item);
    }

}