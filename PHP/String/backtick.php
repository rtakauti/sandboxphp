<?php
echo `touch file1.txt`;
echo `touch file2.txt`;
echo `touch file3.txt`;
echo `touch file4.txt`;
echo `touch file5.txt`;
echo `stat -c "%y %n" *`;
foreach (array_filter(explode("\n", `stat -c "%y %n" *.txt`)) as $item) {
    preg_match("/(\d{4}-\d{2}-\d{2})/", $item, $date);
    preg_match("/(\w+\.txt)/", $item, $file);
    if ((new \DateTime('NOW'))->diff(\DateTime::createFromFormat('Y-m-d', $date[0]))->format('%a') == 0) {
        echo `rm {$file[0]}`;
    }
}
echo `stat -c "%y %n" *`;