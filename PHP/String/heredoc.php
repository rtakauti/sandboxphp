<?php

$str = <<<EOD
Exemplo de uma string
distribuída em várias linhas
utilizando a sintaxe heredoc.
EOD;

class foo
{
    var $foo;
    var $bar;

    function foo()
    {
        $this->foo = 'Foo';
        $this->bar = array('Bar1', 'Bar2', 'Bar3');
    }
}

$foo = new foo();
$name = 'Meu nome';

echo <<<EOT
Meu nome é "$name". Eu estou imprimindo $foo->foo.
Agora, eu estou imprimindo {$foo->bar[1]}.
Isto deve imprimir um 'A' maiúsculo: \x41
EOT;

var_dump(array(<<<EOD
foobar!
EOD
));

function foo()
{
    static $bar = <<<LABEL
Nothing in here...
LABEL;
}

class fooo
{
    const BAR = <<<FOOBAR
Constant example
FOOBAR;

    public $baz = <<<FOOBAR
Property example
FOOBAR;
}

echo fooo::BAR;
echo PHP_EOL;
$fooo = new fooo;
echo $fooo->baz;
echo PHP_EOL;
echo <<<"FOOBAR"
Hello World!
FOOBAR;

?>