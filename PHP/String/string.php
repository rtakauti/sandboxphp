<?php

$juice = "apple";
$apple = "grape";

echo "He drank some $juice juice." . PHP_EOL;
echo "He drank some juice made of ${$juice}s.";

$juices = array("apple", "orange", "koolaid1" => "purple");

echo "He drank some $juices[0] juice." . PHP_EOL;
echo "He drank some $juices[1] juice." . PHP_EOL;
echo "He drank some $juices[koolaid1] juice." . PHP_EOL;

class people
{
    public $john = "John Smith";
    public $jane = "Jane Smith";
    public $robert = "Robert Paulsen";

    public function smith()
    {
        return 'Smith';
    }

}

$people = new people();

echo "$people->john drank some $juices[0] juice." . PHP_EOL;
echo "$people->john then said hello to $people->jane." . PHP_EOL;
echo "$people->john's wife greeted $people->robert." . PHP_EOL;
echo "$people->john's wife greeted {$people->smith()}." . PHP_EOL;


$str = 'This is a test.';
echo $first = $str[0];
echo PHP_EOL;
echo $third = $str[2];
echo PHP_EOL;
echo $str = 'This is still a test.';
echo PHP_EOL;
echo $last = $str[strlen($str)-1];
echo PHP_EOL;
echo $str = 'Look at the sea';
echo PHP_EOL;
$str[strlen($str)-1] = 'e';
echo $str;