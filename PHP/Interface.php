<?php
namespace InterfaceTeste {

    interface Teste
    {
        const TESTE = __CLASS__;
    }
}
namespace Test{

    use InterfaceTeste\Teste;

    class Testando implements Teste
    {
        public function print()
        {
            echo static::TESTE;
        }
    }
}

namespace {

    use InterfaceTeste\Teste;
    use Test\Testando;

    $teste = new Testando();
    $teste->print();
    echo PHP_EOL;
    echo Teste::TESTE;
    echo PHP_EOL;
    echo $teste instanceof Teste;
}
