<?php
function sum(...$numbers) {
    $acc = 0;
    foreach ($numbers as $n) {
        $acc += $n;
    }
    return $acc;
}

echo sum(1, 2, 3, 4);

echo '<hr>';

/**
 *
 */
function foo()
{
	$numargs = func_num_args();
	echo "Number of arguments: $numargs<br />\n";
	if ($numargs >= 2) {
		echo 'Second argument is: ' . func_get_arg (1) . "<br />\n";
	}
	$arg_list = func_get_args();
	for ($i = 0; $i < $numargs; $i++) {
		echo "Argument $i is: " . $arg_list[$i] . "<br />\n";
	}
}

foo (1, 2, 3);

echo '<hr>';

function add($a, $b) {
	return $a + $b;
}

echo add(...[1, 2]). '<br/>';

$a = [1, 2];
echo add(...$a);

echo '<hr>';

function adiciona(...$numbers){
	return array_sum($numbers);
}

echo adiciona(1,2,3,4,5,6);
