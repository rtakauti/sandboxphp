<?php

$test = [
    'qwe' => 'wqerwe',
    'gdfg' => 'fghg',
    'qghjgwe' => 'dfgdf',
    'etre' => 'fhfg',
];

print_r(array_map(static function ($key, $value) {
    return 'value: ' . $value . ' key:' . $key;
},
    array_keys($test),
    $test));

$f = [
    1,
    1,
    1,
    1,
];

$s = [
    2,
    2,
    2,
    2,
    2,
];

$t = [
    3,
    3,
    3,
    3,
    3,
    3,
];

print_r(array_map(static function ($f, $s, $t) {
    return $f . $s . $t;
}, $f, $s, $t));

print_r(array_map(static function ($teste) {
    return [$teste => 1];
}, ['um', 'dois', 'um', 'um']));
