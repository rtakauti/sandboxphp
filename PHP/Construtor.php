<?php

class Construtor
{
    public $construct = '10';
    
    public function __invoke()
    {
        return $this->construct;
    }
    
    public function __toString()
    {
        return $this();
    }
}

$construct = function () {
    return new Construtor;
};

echo $construct()() . PHP_EOL;

echo $construct() .PHP_EOL;

echo (new Construtor())();
