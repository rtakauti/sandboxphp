<?php

class Test
{
    private $data;

    private function __construct($data)
    {
        $this->data = $data;
    }

    public static function init($data)
    {
        return new static($data);
    }

    public function map(callable $callback)
    {
        return static::init($callback($this->data));
    }

    private static function par($data)
    {
        return $data % 2 === 0;
    }

    public static function getPar($data): array
    {
        return array_filter($data, 'self::par');
    }

    public function getParMap()
    {
        return $this->map( [$this, str_replace('Map', '', __FUNCTION__)]);
    }

    public function get()
    {
        return $this->data;
    }

}
$init = Test::init([1, 2, 3, 4, 5, 6]);
print_r($init->getParMap()->map('array_reverse')->get());
//print_r($init);