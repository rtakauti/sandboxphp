<?php

namespace Rtakauti;


class Builder
{

    private $name;
    private $age;
//    private $endereco;

//    public function __construct($endereco)
//    {
//        $this->endereco = $endereco;
//    }


    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function setAge(int $age)
    {
        $this->age = $age;
        return $this;
    }


    public function build(callable $callback)
    {
        $callback($this, 100);
    }


}

$builder = new Builder;
$builder->build(function (Builder $build, $age) {
    $build
        ->setName('Rubens')
        ->setAge($age);
});


print_r($builder);

//$fluent = new Builder('endereco');
//$fluent->setName('Rubens')->setIdade(21);
//
//$build1 = new Builder('endereco');

//$build = new Builder('');
//$build->build($callback = function (Builder $build) {
//    $build
//        ->setName('teste')
//        ->setIdade(10);
//});

//print_r($build);
//$callback->call($build, $build1);
//print_r($build1);

