<?php
function powpow($vector){
	foreach ($vector as $value) {
		yield $value * $value;
	}
}
$vec = [1,2,3,4,5];
$other = powpow($vec);
foreach ($other as $number) {
	echo "$number - ";
}
echo "<hr>";
// Jeito errado porque ele gera o vetor completo e retorna o resultado

function cria($tamanho){
	$dataset=[];
	for ($i = 0; $i <= $tamanho; $i++) {
		$dataset[] = $i;	
	}
	return $dataset;
}
$meu = cria(1000);
foreach ($meu as $value) {
	echo nl2br($value.PHP_EOL);
}

//Jeito certo porque o generator a cada itera��o � pausado e 
//retornado o valor para a fun��o que chamou.

function criaYield($tamanho){
	for ($i = 0; $i <= $tamanho; $i++) {
		yield $i;
	}
}

$meu = criaYield(1000);
foreach ($meu as $value) {
	echo nl2br($value.PHP_EOL);
}


