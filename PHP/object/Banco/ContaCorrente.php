<?php

namespace Banco;
include_once 'Operacoes.php';
include_once 'Operacoes1.php';

use LogicException;

class ContaCorrente extends Conta
{
    private $extrato;

    use Operacoes, Operacoes1{
        Operacoes::solicitarCartao insteadof Operacoes1;
    }

    public function __construct($value)
    {
        $this->extrato = $value;
        parent::__construct($value);
    }

    public function retornaSaldo()
    {
        return $this->getSaldo();
    }

    public function &retornaExtrato()
    {
        return $this->extrato;
    }


    public function retirar($value)
    {
        if ($this->getSaldo() < $value) {
           throw new LogicException('Saldo insuficiente');
        }
        $this->setSaldo($this->getSaldo() - $value);
    }

//    public function solicitarCartao()
//    {
//        return 'Seu cartão foi solicitado pela conta corrente';
//    }


    function sacar()
    {
       return 'sacar';
    }
}