<?php

namespace Banco;


abstract class Conta
{
    private $saldo;

    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }

    public function getSaldo()
    {
        return $this->saldo;
    }

    public function __construct($value)
    {
        $this->saldo = $value;
    }

    public function depositar($value)
    {
        $this->saldo += $value;
    }

    public function mostrarSaldo()
    {
        return $this->saldo;
    }

    abstract function sacar();


}