<?php

use Banco\ContaCorrente;

$extension = 'php';
spl_autoload_register(function ($filename) use ($extension){
    include_once __DIR__.DIRECTORY_SEPARATOR.$filename.'.'.$extension;
});

$contaCorrente = new ContaCorrente(100);
$contaCorrente->depositar(50);
$extrato = &$contaCorrente->retornaExtrato();
//echo $extrato.PHP_EOL;
$extrato += 50;
//echo $contaCorrente->retornaExtrato().PHP_EOL;
//echo $contaCorrente->mostrarSaldo().PHP_EOL;
try{
    $contaCorrente->retirar(180);
}catch (LogicException $exception){
    echo $exception->getMessage().PHP_EOL;
}catch (Exception $exception){
    echo $exception->getMessage();
}finally{
    echo $contaCorrente->mostrarSaldo().PHP_EOL;
}

echo $contaCorrente->solicitarCartao();
