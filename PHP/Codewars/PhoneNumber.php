<?php
declare(strict_types=1);


namespace Rtakauti\Codewars;


class PhoneNumber
{
    function createPhoneNumber($numbersArray): string
    {
        return vsprintf('(%d%d%d) %d%d%d-%d%d%d%d', $numbersArray);
    }
}