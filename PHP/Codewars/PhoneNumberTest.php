<?php

namespace Rtakauti\Codewars;

use PHPUnit\Framework\TestCase;

class PhoneNumberTest extends TestCase
{
    private $phoneNumber;

    public function setUp(): void
    {
        $this->phoneNumber = new PhoneNumber();
        parent::setUp();
    }

    public function testCreatePhoneNumber()
    {
        $this->assertEquals('(123) 456-7890', $this->phoneNumber->createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]));
        $this->assertEquals('(111) 111-1111', $this->phoneNumber->createPhoneNumber([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]));
    }

}
