<?php

/**
 * Classe TO de Usuario
 * @author Rubens
 *
 */
class User
{
    
    private $nome;
    private $sobrenome;
    
    
    

    /**
     * nome
     * @return string
     */
    public function getNome(){
        return $this->nome;
    }

    /**
     * nome
     * @param string $nome
     * @return Classe
     */
    public function setNome($nome){
        $this->nome = $nome;
        return $this;
    }

    /**
     * sobrenome
     * @return string
     */
    public function getSobrenome(){
        return $this->sobrenome;
    }

    /**
     * sobrenome
     * @param string $sobrenome
     * @return Classe
     */
    public function setSobrenome($sobrenome){
        $this->sobrenome = $sobrenome;
        return $this;
    }

}