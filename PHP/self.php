<?php

class Avo
{
    protected static $qtd = 0;
    
    public function __construct()
    {
        ++self::$qtd;
    }
    
    public static function getQtd()
    {
        return static::$qtd;
    }
}

class Pai extends Avo
{
    protected static $qtd = 0;
    
    public function __construct()
    {
        parent::__construct();
        ++self::$qtd;
    }
}

class Filho extends Pai
{
    protected static $qtd = 0;
    
    public function __construct()
    {
        parent::__construct();
        ++self::$qtd;
    }
}

class Neto extends Filho
{
    protected static $qtd = 0;
    
    public function __construct()
    {
        parent::__construct();
        ++self::$qtd;
    }
}

$filho = new Filho();
$pai   = new Pai();
$avo   = new Avo();
$neto  = new Neto();

echo 'Avo: ' . Avo::getQtd() . PHP_EOL;
echo 'Pai: ' . Pai::getQtd() . PHP_EOL;
echo 'Filho: ' . Filho::getQtd() . PHP_EOL;
echo 'Neto: ' . Neto::getQtd() . PHP_EOL;
