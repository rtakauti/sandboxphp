<?php

namespace Rtakauti\Functional;

use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{

    private $container;
    private $string;

    public function setUp(): void
    {
        $this->string = 'Test';
        $this->container = Container::of($this->string);
        parent::setUp();
    }

    public function testOf(): void
    {
        $this->assertEquals(Container::of($this->string), $this->container);
    }

    public function test__toString(): void
    {
        ob_start();
        echo $this->container;
        $stringTest = ob_get_flush();
        $this->assertEquals(Container::of($this->string)->__toString(), "Container[ {$this->string} ]");
        $this->assertEquals($stringTest, "Container[ {$this->string} ]");
    }

    public function test__invoke(): void
    {
        $containerTest = $this->container;
        $this->assertEquals($this->container->__invoke(), $this->string);
        $this->assertEquals($containerTest(), $this->string);
    }

    public function testMap(): void
    {
        $this->assertEquals($this->container->map('strtoupper'), Container::of(strtoupper($this->string)));
    }

    public function testToUpper(): void
    {
        $this->assertEquals($this->container->map('Rtakauti\Functional\Container::toUpper')(), strtoupper($this->string));
    }

    public function testToLower(): void
    {
        $this->assertEquals(strtolower($this->string), $this->container->toLower());
    }

    public function testGet(): void
    {
        $this->assertEquals($this->container->get(), $this->string);
    }
}
