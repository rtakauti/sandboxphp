<?php
declare(strict_types=1);
$array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

function square(int $num): int
{
    return $num ** 2;
}

function add($a, int $b): int
{
    return $a + $b;
}

print_r(array_map('square', $array));

echo array_reduce(array_map('square', $array), 'add') . PHP_EOL;

//--------------------------------------------------------------------

function concatWith(string $a): callable
{
    return static function (string $b) use ($a): string {
        return $a . $b;
    };
}

$helloWith = concatWith('Hello ');
echo $helloWith('World') . PHP_EOL;

//-----------------------------------------------------------------------------

function apply1(callable $operator, $a, $b)
{
    return $operator($a, $b);
}

$add = static function (float $a, float $b): float {
    return $a + $b;
};

$divide = static function (float $a, float $b): float {
    return $a / $b;
};

echo apply1($add, 5, 5) . PHP_EOL;
echo apply1($divide, 5, 5) . PHP_EOL;


//-----------------------------------------------------------------------------

function apply(callable $operator): callable
{
    return static function ($a, $b) use ($operator) {
        return $operator($a, $b);
    };
}

echo apply($add)(5, 5) . PHP_EOL;
echo apply($divide)(5, 5) . PHP_EOL;

$adder = apply($add);
$divider = apply($divide);
echo $adder(5, 5) . PHP_EOL;
echo $divider(5, 5) . PHP_EOL;

function safeDivide(float $a, float $b): float
{
    return empty($b) ? NAN : $a / $b;
}

echo apply('safeDivide')(5, 0) . PHP_EOL;


class Counter
{
    private $_value;

    public function __construct($init)
    {
        $this->_value = $init;
    }

    public function __invoke()
    {
        return $this->increment();
    }

    public function increment(): int
    {
        return $this->_value++;
    }
}

$c = new Counter(0);
echo $c() . PHP_EOL;
echo $c() . PHP_EOL;
echo $c() . PHP_EOL;