<?php
declare(strict_types=1);

namespace Rtakauti\Functional;

class Container
{
    private $_value;

    private function __construct($value)
    {
        $this->_value = $value;
    }

    public static function toUpper($value): string
    {
        return strtoupper($value);
    }

    public function toLower(): string
    {
        return static::of(strtolower($this->_value))->get();
    }

    public static function of($value): self
    {
        return new static($value);
    }

    public function map(callable $callable): self
    {
        return static::of($callable($this->_value));
    }

    public function __toString(): string
    {
        return "Container[ {$this->_value} ]";
    }

    public function __invoke()
    {
        return $this->_value;
    }

    public function get()
    {
        return $this->_value;
    }
}