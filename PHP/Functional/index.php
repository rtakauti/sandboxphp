<?php
declare(strict_types=1);

use Rtakauti\Functional\Container;

include_once __DIR__ . '/../../vendor/autoload.php';


print_r(array_map('htmlspecialchars', ['</ HELLO >']));

//---------------------------------------------------------------------

function container_map(callable $f, Container $c): Container
{
    return $c->map($f);
}

$container = Container::of('</ HELLO >');
print_r(container_map('htmlspecialchars', $container));

$c = Container::of('</ Hello FP >')->map('htmlspecialchars')->map('strtolower');
echo $c . PHP_EOL;

$repeat = static function ($a) {
    return static function ($b) use ($a) {
        return $a * $b;
    };
};
$c = Container::of('Hello FP')->map('strlen')->map($repeat(2));
echo $c() . PHP_EOL;

$c = Container::of('Hello FP')->map('Rtakauti\Functional\Container::toUpper');
echo $c() . PHP_EOL;

$c = Container::of([1, 2, 3])->map('array_reverse');
print_r($c());


//-------------------------------------------------------------------------------

function addTo($a)
{
    return static function ($b) use ($a) {
        return $a + $b;
    };
}

$filter = function (callable $callable): Container {
    return Container::of($callable($this->_value) ? $this->_value : 0);
};

$wrappedInput = Container::of(2);

$validate = $filter->bindTo($wrappedInput, 'Rtakauti\Functional\Container');
echo $validate('is_numeric')->map(addTo(40));

