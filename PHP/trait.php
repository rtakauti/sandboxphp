<?php
abstract class Animal{
	
}

abstract class Cao extends Animal{
	public function morder(){
		return  'Nhac';
	}
}

abstract class Gato extends Animal{
	
}

final class CaoSelvagem extends Cao{
	use Selvagem;
}

final class CaoDomesticado extends Cao{
	use Domesticado;
}

final class CaoSelvagemDomesticado extends Cao{
	use Selvagem, Domesticado{
		Domesticado::morder insteadof Selvagem;
		Selvagem::morder as morderForte;
	}
}

final class GatoSelvagem extends Gato{
	
}

final class GatoDomesticado extends Gato{
	
}

trait Selvagem{
	public function morder(){
		return  '<b>Nhac</i>';
	}
}

trait Domesticado{
	public function morder(){
		return  '<i>Nhac</i>';
	}
}

$caoSelvagem = new CaoSelvagem();
$caoDomesticado = new CaoDomesticado();
$caoSelvagemDomesticado = new CaoSelvagemDomesticado();

echo $caoSelvagem->morder();
echo "<br>";
echo $caoDomesticado->morder();
echo "<br>";
echo $caoSelvagemDomesticado->morder();
echo "<br>";
echo $caoSelvagemDomesticado->morderForte();

echo "<hr>";


trait Parafuso{
	abstract public function ativar();
}

trait Motor{
	use Parafuso;
	
	protected $tanqueCheio;
	
	private function ligar() {

	}
}

abstract class Automovel{
	use Motor{
		Motor::ligar as public ligarMotor; 
		Motor::ligar as public ligar1;
	}
}

final class Carro extends Automovel{
	public function ativar(){
		
	}
}

$carro = new Carro();
$carro->ligarMotor();
$carro->ligar1();


echo "<hr>";

trait Test{
	static public function retornar($string){
		return $string;
	}
}
class Classe{
	use Test;
}

echo Classe::retornar('Teste da trait');



