<?php

namespace CoffeeShop;


class Milk extends CondimentDecorator
{

    private $beverage;

    public function __construct(Beverage $beverage)
    {
        $this->beverage = $beverage;
    }

    public function cost()
    {
        return .10 + $this->beverage->cost();
    }

    public function getDescription()
    {
        return $this->beverage->getDescription(). ', Milk';
    }
}