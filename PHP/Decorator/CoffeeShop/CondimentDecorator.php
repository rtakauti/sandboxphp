<?php

namespace CoffeeShop;


abstract class CondimentDecorator extends Beverage
{
    public function getDescription()
    {
        return $this->description;
    }
}