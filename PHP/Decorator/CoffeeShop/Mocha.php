<?php

namespace CoffeeShop;


class Mocha extends CondimentDecorator
{

    private $beverage;

    public function __construct(Beverage $beverage)
    {
        $this->beverage = $beverage;
    }

    public function cost()
    {
        return .20 + $this->beverage->cost();
    }

    public function getDescription()
    {
        return $this->beverage->getDescription(). ', Mocha';
    }
}