<?php

use CoffeeShop\Espresso;
use CoffeeShop\HouseBlend;
use CoffeeShop\Milk;
use CoffeeShop\Mocha;


spl_autoload_register(function ($filename) {
    include_once __DIR__ . DIRECTORY_SEPARATOR . $filename . '.php';
});
$espresso = new Espresso();
echo $espresso->getDescription().' '.$espresso->cost().PHP_EOL;

$houseBlend = new HouseBlend();
echo $houseBlend->getDescription().' '.$houseBlend->cost().PHP_EOL;
$houseBlend = new Milk($houseBlend);
echo $houseBlend->getDescription().' '.$houseBlend->cost().PHP_EOL;
$houseBlend = new Mocha($houseBlend);
echo $houseBlend->getDescription().' '.$houseBlend->cost().PHP_EOL;
