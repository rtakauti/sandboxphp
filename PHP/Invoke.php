<?php

namespace Teste {
    
    class Invoke
    {
        const ADD = 'add';
        
        private $three;
        private $two;
        private $one;
        private $first;
        
        public function get()
        {
            return $this([$this, 'add'], [$this, 'add'], [$this, 'add'])(1)(1, 2, 3, 4)(1, 2);
        }
        
        public function add($item): int
        {
            return array_sum($item);
        }
        
        public function one($item)
        {
            $this->first = function ($one) use ($item) {
                return $one + $item;
            };
            
            return $this->first;
        }
        
        public function first()
        {
            return $this->first;
        }
        
        public function second($item)
        {
            $teste = $this->first;
            
            return $teste($item);
        }
        
        public function __invoke(callable ...$callable)
        {
            $this->one = function (...$one) use ($callable) {
                $this->two = function (...$two) use ($callable, $one) {
                    $this->three = function (...$three) use ($callable, $one, $two) {
                        return $callable[0]($three) + $callable[1]($two) + $callable[2]($one);
                    };
//                    $test        = $this->three;
//                    echo $test() . PHP_EOL;
                    
                    return $this->three;
                };
//                $teste      = $this->two;
//                echo $teste()() . PHP_EOL;
                
                return $this->two;
            };
//            $testee      = $this->one;
//            echo $testee()()() . PHP_EOL;
            
            return $this->one;
        }
    }
}

namespace Teste1 {

    use Teste\Invoke;

    $invoke = new Invoke();
    
    echo $invoke->get() . PHP_EOL;
    
    echo $invoke->add([1, 2]) . PHP_EOL;
    
    echo $invoke([$invoke, Invoke::ADD], [$invoke, Invoke::ADD], [$invoke, Invoke::ADD])(1, 2)(1, 2)(1, 2) . PHP_EOL;
    
    echo $invoke->one(3)(100) . PHP_EOL;
    
    echo $invoke->first()(1000) . PHP_EOL;
    
    echo $invoke->second(10) . PHP_EOL;
}