<?php


namespace Math;


class Triangle
{
    private $a;
    private $b;
    private $c;

    public function __construct(int $a, int $b, int $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    public function getShape(): string
    {
        $triangle = 'Escaleno';
        if ($this->a === $this->b && $this->b === $this->c) {
            $triangle = 'Equilátero';
        }elseif ($this->a === $this->b || $this->b === $this->c || $this->a === $this->c){
            $triangle = 'Isósceles';
        }
        return $triangle;
    }
}

echo 'Lado A: ';
fscanf(STDIN, "%d\n", $a);
echo 'Lado B: ';
fscanf(STDIN, "%d\n", $b);
echo 'Lado C: ';
fscanf(STDIN, "%d\n", $c);

echo 'O triangulo é: ', (new Triangle($a, $b, $c))->getShape();