<?php


namespace Rtakauti\LeetCode;

/*
Given an array of integers, return indices of the two numbers such that they add up to a specific target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
Example:
Given nums = [2, 7, 11, 15], target = 9,
Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
 */

/**
 * Class TwoSum
 * @package Rtakauti\LeetCode
 */
class TwoSum
{

    private $nums = [2, 7, 11, 15];
    private $target = 9;

    public function execute()
    {
        $control = $result = [];
        foreach ($this->nums as $key => $num) {
            $complement = $this->target - $num;
            if (array_key_exists($complement, $control)) {
                $result[] = [$control[$complement], $key];
            }
            $control[$num] = $key;
        }
        return $result;
    }
}