<?php

namespace Rtakauti\LeetCode;

use PHPUnit\Framework\TestCase;

class TwoSumTest extends TestCase
{
    private TwoSum $twoSum;
    private array $expected;

    public function setUp(): void
    {
        $this->expected = [[0, 1]];
        $this->twoSum = new TwoSum();
        parent::setUp();
    }

    public function testExecute()
    {
        $this->assertEquals($this->expected, $this->twoSum->execute());
    }
}
