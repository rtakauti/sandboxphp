<?php

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://hooks.slack.com/services/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");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
    '{"text":"Hello World from PHP curl"}');
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
$result = curl_exec($ch);
curl_close($ch);