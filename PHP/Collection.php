<?php

namespace Rtakauti;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;

class Collection implements ArrayAccess, Countable, IteratorAggregate
{
    const MAX = 'max';
    const MIN = 'min';
    const ASC = 'ascendant';
    const DESC = 'descendant';
    public static $c = 0;
    protected $items;
    protected $position;

    public function __construct(array $items = [])
    {
        $this->position = 0;
        $this->items = $items;
    }

    public function offsetExists($offset)
    {
        if (is_integer($offset) || is_string($offset)) {
            return array_key_exists($offset, $this->items);
        }

        return false;
    }

    public function offsetGet($offset)
    {
        return $this->items[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }

    public function count()
    {
        return count($this->items);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    public function toArray()
    {
        return $this->items;
    }

}

//$collection = new \Rtakauti\Collection;

function loop()
{
    $collection = new \Rtakauti\Collection;
    $collection['sfsdfs'] = 'teste1';
    $collection['ads'] = 'teste2';
    $collection::$c += 1;
    echo $collection::$c . "\n";
    return $collection;
}

function teste()
{
    return 'teste';
}

$counter = 0;
function vetor(&$counter)
{
    $counter++;
    return [1, 2, 3, 4, 5];
}

//print_r($collection->toArray());
//print_r(array_values($collection->toArray()));
foreach ($vetor = vetor($counter) as $item) {
    echo $item . "\n";
}

foreach ($collection = loop() as $index => $item) {
    echo $item . "\n";
}

print_r($vetor);
echo $counter . "\n";
if (!empty($test = teste())) {
    echo $test;
}
//echo $collection::$c . "\n";
