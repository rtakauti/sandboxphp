<?php

namespace Number;


class PrimeNumber
{

    public function isPrime($number)
    {
        if (4 === $number) {
            return false;
        }
        if (2 === $number) {
            return true;
        }
        for ($i = 2; $i < sqrt($number); $i++) {
            if (fmod($number, $i) == 0) {
                return false;
            }
        }

        return true;

    }

}

$prime = new PrimeNumber;
for ($i = 2; $i < 1000000; $i++) {
    if ($prime->isPrime($i)) {
        echo $i . " prime\n";
    }
}