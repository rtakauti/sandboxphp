<?php

$jonathan = new stdClass;
$jonathan->name = 'Jonathan';
$jonathan->id = 5;

$abdul = new stdClass;
$abdul->name = 'Abdul';
$abdul->id = 22;

$jamal = new stdClass;
$jamal->name = 'Jamal';
$jamal->id = 21;

$array[] = $jonathan;
$array[] = $abdul;
$array[] = $jamal;


class UserFilter extends FilterIterator
{
    private $string;

    public function __construct(Iterator $iterator , $string )
    {
        parent::__construct($iterator);
        $this->string = $string;
    }

    public function accept()
    {

        return $this->current()->name !== $this->string;
    }
}

function double($data){
     $data->id *= 2;
     return $data;
}

class ArrayCallbackIterator extends ArrayIterator {
  private $callback;
  public function __construct($value, $callback) {
    parent::__construct($value);
    $this->callback = $callback;
  }

  public function current() {
    $value = parent::current();
    return call_user_func($this->callback, $value);
  }
}


$object = new ArrayObject($array);
$iterator = new ArrayCallbackIterator($object, 'double');
echo (new ReflectionClass($object->getIterator()))->getShortName().PHP_EOL;
//$iterator = new UserFilter($object->getIterator(),'Abdul');
$iterator = new UserFilter($iterator,'Abdul');

class Obj implements ArrayAccess,Iterator  {
    private $container;
    private $position;

    public function __construct($container) {
        $this->position = 0;
        $this->container =$container;
    }

    public function offsetSet($offset, $value) {
        if (null === $offset) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->container[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->container[$this->position]);
    }
}

//$object = new Obj($array);

//$iterator = new UserFilter($object,'abdul');

echo $object->count().PHP_EOL;
foreach ($iterator as $result) {
    print_r($result);
}



$object =[];
foreach ($iterator as $value) {
    $object[] = $value;
}


$array = array_filter((array)$iterator, function($obj){
    return true;
});
