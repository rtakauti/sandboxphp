<?php
$a = $b = $c = true;
echo 'A AND B', PHP_EOL;
echo 'true AND true = ', true && true ? 'true' : 'false', PHP_EOL;
echo 'true AND false = ', true && false ? 'true' : 'false', PHP_EOL;
echo 'false AND true = ', false && true ? 'true' : 'false', PHP_EOL;
echo 'false AND false = ', false && false ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo 'A OR B', PHP_EOL;
echo 'true OR true = ', true || true ? 'true' : 'false', PHP_EOL;
echo 'true OR false = ', true || false ? 'true' : 'false', PHP_EOL;
echo 'false OR true = ', false || true ? 'true' : 'false', PHP_EOL;
echo 'false OR false = ', false || false ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo 'NOT', PHP_EOL;
echo 'NOT true = ', !true ? 'true' : 'false', PHP_EOL;
echo 'NOT false = ', !false ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo 'A XOR B', PHP_EOL;
echo 'true XOR true = ', true ^ true ? 'true' : 'false', PHP_EOL;
echo 'true XOR false = ', true ^ false ? 'true' : 'false', PHP_EOL;
echo 'false XOR true = ', false ^ true ? 'true' : 'false', PHP_EOL;
echo 'false XOR false = ', false ^ false ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo '!(A XOR B) = A XNOR B', PHP_EOL;
echo 'true XNOR true = ', !(true ^ true) ? 'true' : 'false', PHP_EOL;
echo 'true XNOR false = ', !(true ^ false) ? 'true' : 'false', PHP_EOL;
echo 'false XNOR true = ', !(false ^ true) ? 'true' : 'false', PHP_EOL;
echo 'false XNOR false = ', !(false ^ false) ? 'true' : 'false', PHP_EOL;

echo PHP_EOL, 'De Morgan\'s Theorem';

echo '!(!A AND !B) = A OR B', PHP_EOL;
echo '!(!true AND !true) = ', !(!true && !true) ? 'true' : 'false', PHP_EOL;
echo '!(!true AND !false) = ', !(!true && !false) ? 'true' : 'false', PHP_EOL;
echo '!(!false AND !true) = ', !(!false && !true) ? 'true' : 'false', PHP_EOL;
echo '!(!false AND !false) = ', !(!false && !false) ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo '!(!A OR !B) = A AND B', PHP_EOL;
echo '!(!true OR !true) = ', !(!true || !true) ? 'true' : 'false', PHP_EOL;
echo '!(!true OR !false) = ', !(!true || !false) ? 'true' : 'false', PHP_EOL;
echo '!(!false OR !true) = ', !(!false || !true) ? 'true' : 'false', PHP_EOL;
echo '!(!false OR !false) = ', !(!false || !false) ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo 'A OR B OR C', PHP_EOL;
echo 'true OR true OR true = ', true || true || true ? 'true' : 'false', PHP_EOL;
echo 'true OR false OR true = ', true || false || true ? 'true' : 'false', PHP_EOL;
echo 'true OR true OR false = ', true || true || false ? 'true' : 'false', PHP_EOL;
echo 'true OR false OR false = ', true || false || false ? 'true' : 'false', PHP_EOL;
echo 'false OR false OR false = ', false || false || false ? 'true' : 'false', PHP_EOL;
echo 'false OR false OR true = ', false || false || true ? 'true' : 'false', PHP_EOL;
echo 'false OR true OR false = ', false || true || false ? 'true' : 'false', PHP_EOL;
echo 'false OR true OR true = ', false || true || true ? 'true' : 'false', PHP_EOL;

echo PHP_EOL,'Simplification with distributive law',PHP_EOL;

echo '!A.!B.C+!A.B.!C+A.!B.!C+A.B.C = ',
!$a && !$b && $c ||
!$a && $b && !$c ||
$a && !$b && !$c ||
$a && $b && $c
    ? 'true' : 'false', PHP_EOL;

echo '!A.(!B.C+B.!C)+A.!B.!C+A.B.C = ',
!$a && (!$b && $c || $b && !$c) ||
$a && !$b && !$c ||
$a && $b && $c
    ? 'true' : 'false', PHP_EOL;

echo '!A.(!B.C+B.!C)+A.(!B.!C+B.C) = ',
!$a && (!$b && $c || $b && !$c) ||
$a && (!$b && !$c || $b && $c)
    ? 'true' : 'false', PHP_EOL;

echo '!A.(!B.C+B.!C)+A.(!B.!C+B.C) = ',
!$a && (!$b && $c || $b && !$c) ||
$a && (!$b && !$c || $b && $c)
    ? 'true' : 'false', PHP_EOL;

echo '!A.(B XOR C)+A.(!B.!C+B.C) = ',
!$a && ($b ^ $c) ||
$a && (!$b && !$c || $b && $c)
    ? 'true' : 'false', PHP_EOL;

echo '!A.(B XOR C)+A.!(B XOR C) = ',
!$a && ($b ^ $c) || $a && !($b ^ $c)
    ? 'true' : 'false', PHP_EOL;

echo 'A XOR B XOR C = ',
$a ^ $b ^ $c ? 'true' : 'false', PHP_EOL;

echo PHP_EOL;

echo 'A XOR B XOR C', PHP_EOL;
echo 'true XOR true XOR true = ', true ^ true ^ true ? 'true' : 'false', PHP_EOL;
echo 'true XOR false XOR true = ', true ^ false ^ true ? 'true' : 'false', PHP_EOL;
echo 'true XOR true XOR false = ', true ^ true ^ false ? 'true' : 'false', PHP_EOL;
echo 'true XOR false XOR false = ', true ^ false ^ false ? 'true' : 'false', PHP_EOL;
echo 'false XOR false XOR false = ', false ^ false ^ false ? 'true' : 'false', PHP_EOL;
echo 'false XOR false XOR true = ', false ^ false ^ true ? 'true' : 'false', PHP_EOL;
echo 'false XOR true XOR false = ', false ^ true ^ false ? 'true' : 'false', PHP_EOL;
echo 'false XOR true XOR true = ', false ^ true ^ true ? 'true' : 'false', PHP_EOL;