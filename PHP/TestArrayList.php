<?php
/*
 * Creating an ArrayList and assinging, then removing certain objects.
 */
//Import ArrayList lib
require __DIR__.'/ArrayList.php';
//Create our class
class MyClass {
    public $name;
    public $n;

    public function __construct($name, $n) {
        $this->name = $name;
        $this->n = $n;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getN()
    {
        return $this->n;
    }


}
//Create our Objects
$object1 = new MyClass('Hello', 2);
$object2 = new MyClass('World', 12);
$object3 = new MyClass('How', 21);
$object4 = new MyClass('Are', 23);
$object5 = new MyClass('You', 42);
//Create our ArrayList
$myList = new ArrayList('MyClass');
$myList->put(null, $object1);
$myList->put(null, $object2);
$myList->put(null, $object3);
$myList->put(null, $object4);
$myList->put(null, $object5);
//Print the list as-is
echo "List 1:\n" . (string)$myList . "\n\n";
//Will Print: [{"name": "Hello"}, {"name": "World"}, {"name": "How"}, {"name": "Are"}, {"name": "You"}]
$myList->forget($object2);
echo "List 2:\n" . (string)$myList . "\n\n";

for($i = 0; $i < $myList->count(); $i++) {
    echo $myList[$i]->name . ' ';
}

echo "\n\n";
//Will Print: "Hello World How Are You "
foreach($myList as $obj) {
    echo $obj->name . ' ';
}
echo "\n\nmax";
print_r($myList->max('n'));
echo "\n\nmin";
print_r($myList->min('n'));
//echo "\n\nvalues";
//print_r($myList->values());
echo "\n\nmap";
print_r($myList->map(function($list){
    return $list->n *2;
}));
echo "\n\nfilter";
print_r($myList->filter(function($list){
    return $list->n === 2;
}));
echo "\n\nsort";
print_r($myList->sort(function ($a, $b) {
    if ($a->n === $b->n) {
        return 0;
    }
    return ($a->n > $b->n) ? -1 : 1;
}));

echo $myList->isEmpty();
print_r($myList->reverse());
echo $myList->contains($object4);

print_r($myList->pop());
echo "\n\nsjson";
print_r($myList->jsonSerialize());