<?php

class A
{
    public $one = '';
    public $two = '';

    //Constructor
    public function __construct()
    {
        //Constructor
    }

    //print variable one
    public function echoOne()
    {
        echo $this->one."\n";
    }

    //print variable two
    public function echoTwo()
    {
        echo $this->two."\n";
    }
}

//Instantiate the object
$a = new A();

//Instantiate the reflection object
$reflector = new ReflectionClass('A');

//Now get all the properties from class A in to $properties array
$properties = $reflector->getProperties();

$i =1;
//Now go through the $properties array and populate each property
foreach($properties as $property)
{
    //Populating properties
    $a->{$property->getName()}=$i;
    //Invoking the method to print what was populated
    $a->{"echo".ucfirst($property->getName())}()."\n";
    echo ucfirst($property->getName());
    $i++;
}

class Foo {
    public    $p1;
    protected $p2;
    private   $p3;

    /**
     * @return mixed
     */
    public function getP1()
    {
        return $this->p1;
    }

    /**
     * @return mixed
     */
    public function getP2()
    {
        return $this->p2;
    }

    /**
     * @return mixed
     */
    public function getP3()
    {
        return $this->p3;
    }



}

$obj = new ReflectionObject(new Foo());

$props =array_combine($obj->getProperties(), $obj->getMethods());
foreach ($props as $key => $item) {
    echo $key.' '.$item;
}

var_dump($obj->hasProperty("p1"));
var_dump($obj->hasProperty("p2"));
var_dump($obj->hasProperty("p3"));
var_dump($obj->hasProperty("p4"));
