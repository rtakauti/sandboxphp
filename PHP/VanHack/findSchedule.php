<?php
function permute($values, $constants, &$combinations, $items, $keys = [])
{
    if (empty($items)) {
        $array = array_combine($keys, $values);
        foreach ($constants as $key => $constant) {
            $array[$key] = $constant;
        }
        for ($i = 0; $i < count($array); $i++) {
            $combination[$i] = $array[$i];
        }
        if (!in_array($combination = implode('',$combination), $combinations)) {
            $combinations[] = $combination;
        }

    } else {
        for ($i = count($items) - 1; $i >= 0; --$i) {
            $newitems = $items;
            $newperms = $keys;
            list($foo) = array_splice($newitems, $i, 1);
            array_unshift($newperms, $foo);
            permute($values, $constants, $combinations,$newitems, $newperms);
        }
    }
}

function findSchedules($workHours, $dayHours, $pattern)
{
    $days = str_split($pattern);
    $countMask = substr_count($pattern, '?');
    $hours = $workHours - array_sum($days);
    $workDays = [];
    $keys = [];
    $constants = [];
    $combinations = [];
    if ($hours < $dayHours) {
        $workDays[] = $hours;
    }
    while (($hours = $hours - $dayHours) >= 0) {
        $workDays[] = $dayHours;
    }
    $values = $workDays;
    if (($count = count($workDays)) < $countMask) {
        $values = array_merge($workDays, array_fill(0, $countMask - $count, 0));
    }
    foreach ($days as $key => $day) {
        if ('?' == $day) {
            $keys[] = $key;
        } else {
            $constants[$key] = $day;
        }
    }

    permute($values, $constants, $combinations,$keys);
    return array_reverse($combinations);
}

//print_r(findSchedule(56, 8, '???8???'));
//echo "\n";
//8888888;
//print_r(findSchedule(3, 2, '??2??00'));
/*
0020100
0021000
0120000
1020000
 */
print_r(findSchedule(3, 1, '???????'));
/*
*0000111
*0001011
*0001101
*0001110
*0010011
*0010101
*0010110
*0011001
*0011010
*0011100
*0100011
*0100101
*0100110
*0101001
*0101010
*0101100
*0110001
*0110010
*0110100
*0111000
*1000011
*1000101
*1000110
*1001001
*1001010
*1001100
*1010001
*1010010
*1010100
*1011000
*1100001
*1100010
*1100100
*1101000
*1110000
 */